/*global module:false*/
module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            front: {
                src: [
                    'js/front/lib/angular.js',
                    'js/front/MovieApp.js',
                    'js/front/MovieService.js',
                    'js/front/MoviesController.js'
                ],
                dest: 'js/movies.js'
            }
        },
        uglify: {
            dist: {
                src: [
                    'js/front/lib/angular.js',
                    'js/front/MovieApp.js',
                    'js/front/MovieService.js',
                    'js/front/MoviesController.js'
                ],
                dest: 'js/movies.js'
            }
        },
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                unused: true,
                boss: true,
                eqnull: true,
                globals: {}
            },
            code: {
                src: ['js/**/*.js', '!js/movies.js', '!js/**/lib/**/*.js']
            }
        },
        sass: {
            default: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: {
                    'css/admin.css': 'styles/admin.scss',
                    'css/style.css': 'styles/style.scss'
                }
            }
        },
        copy: {
            dist: {
                files: [
                    {expand: true, src: ['css/*.css'], dest: 'dist/movie-sites/', filter: 'isFile'},
                    {expand: true, src: ['js/movies.js'], dest: 'dist/movie-sites/', filter: 'isFile'},
                    {expand: true, src: ['images/*'], dest: 'dist/movie-sites/', filter: 'isFile'},
                    {expand: true, src: ['html/*'], dest: 'dist/movie-sites/', filter: 'isFile'},
                    {expand: true, src: ['*.php'], dest: 'dist/movie-sites/', filter: 'isFile'}
                ]
            }
        },
        jasmine: {
            default: {
                src: [
                    'js/front/lib/*.js',
                    'spec/helpers/angular-mocks.js',
                    '!js/movies.js',
                    'js/front/MovieApp.js',
                    'js/front/MovieService.js',
                    'js/front/MoviesController.js'
                ],
                options: {
                    specs: 'spec/*Spec.js',
                    keepRunner: true,
                    outfile: '_SpecRunner.html',
                    template: require('grunt-template-jasmine-istanbul'),
                    templateOptions: {
                        files: [
                            'js/front/lib/angular.js',
                            'js/front/MovieApp.js',
                            'js/front/MovieService.js',
                            'js/front/MoviesController.js',
                            '!js/movies.js'],
                        coverage: 'bin/coverage/coverage.json',
                        report: 'bin/coverage'
                    }
                }
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');

    // Default task.
    grunt.registerTask('default', ['jshint', 'sass', 'concat', 'uglify']);

};
