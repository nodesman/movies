(function () {
    var module = angular.module('MovieApp');
    module.factory('movieService', ['$http', 'rootUrl', function ($http, rootUrl) {
        return {
            fetchMovies: function (callback) {
                $http.get(rootUrl + '/?movies=1')
                    .then(function (response) {
                        callback(response.data, null);
                    }, function (response) {
                        callback(null, response);
                    });
            }
        };
    }]);
})();