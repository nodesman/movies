(function () {
    var module = angular.module('MovieApp');
    module.controller('MovieController', ["$scope", "movieService", function ($scope, $movieService) {
        $scope.loading = true;
        $scope.ready = false;
        $scope.list = [];

        function generateArray(number) {
            var result = [];
            for (var iter = 0; iter < number; iter++) {
                result.push(1);
            }
            return result;
        }

        $scope.ajaxCallback = function (data, error) {
            $scope.loading = false;
            $scope.ready = true;
            angular.copy(data.data, $scope.list);
            for (var iter in $scope.list) {
                $scope.list[iter].rating = generateArray($scope.list[iter].rating);
            }
        };
        $movieService.fetchMovies($scope.ajaxCallback);
    }]);
})();