Prerequisites
=============

1. PHP v5.6
2. Ruby v2.2
3. Ruby sass gem v3.4
4. nodejs v0.12
5. Node grunt-cli module installed globally

Grunt is the primary task runner for this project.

1. Set up
======================

Clone the repository into the wp-content/plugins directory of your wordpress installation. If you want to install a new wordpress installation, use your [wp-cli](http://wp-cli.org/) that you have [installed](http://wp-cli.org/):

```bash
dev:wordpress $ wp core download
dev:wordpress $ cd wp-content/plugins
dev:wordpress $ git clone git@bitbucket.org:nodesman/movies.git
```

2. Install development dependencies
===================================

* Install required dependencies

```bash
$ composer install
```

* Install required node modules

```bash
$ npm install
```

* Install test version of wordpress

```bash 
$ bin/install-wp-tests.sh wordpress_test root '' localhost latest
```

Where wordpress_test is the database schema you are going to test against, root is the mysql database username "" is where the password for the mysql database user password for `root` and latest is the version of wordpress you are going to install.

3. Building
============
Run PHPUnit tests like so:

```bash
$ phpunit
```

View coverage inforation by running PHPUnit with the following:

```bash
$ phpunit --coverage-html ./report
```

Run Javascript unit tests using jasmine tests like so:

```bash
$ grunt jasmine 
```

This will, in addition to running tests, write out a coverage report at bin/coverage/index.html. View coverage report at that page. 

Build assets like so:

```bash
$ grunt sass uglify
```

Create a deployable build of the plugin:

```bash
$ grunt copy
```

You will find the deployable copy of the plugin under dist/ of the repository root. 

