<?php

class FrontPageRenderer
{
    function __construct()
    {
        add_action('init', array($this, 'init'));
    }

    function init()
    {
        if (isset($_GET['movies']) && $_GET['movies'] == 1) {
            $this->renderMoviesJSON();
            exit;
        }
        add_filter('the_title', array($this, 'the_title'));
        add_filter('the_content', array($this, 'the_content'));
        add_filter('wp_footer', array($this, 'footer'));
        add_filter('wp_head', array($this, 'header'));
    }

    function renderMoviesJSON()
    {
        $posts = get_posts(array(
            'numberofposts' => -1,
            'post_type' => 'movie'
        ));

        $json_model = (object)["data" => []];
        foreach ($posts as $post) {
            $json_model->data[] = (object)array(
                "id" => $post->ID,
                "title" => $post->post_title,
                "short_description" => $post->post_excerpt,
                "rating" => intval(get_post_meta($post->ID, "movie_rating", true)),
                "poster_url" => get_post_meta($post->ID, "movie_url", true),
                "year" => intval(get_post_meta($post->ID, "movie_year", true))
            );
        }
        echo json_encode($json_model);
    }

    function header()
    {
        if (Movies::configuredForMovieListing()) {
            require('html/front-page-header.html');
        }
    }

    function footer()
    {
        if ($this->isPageOnFront()) {
            require("html/front-page-footer.html");
        }
    }

    function the_title($title)
    {
        if ($this->isPageOnFront()) {
            return __("Movies you've watched");
        }
        return $title;
    }

    function the_content($content)
    {
        if ($this->isPageOnFront()) {
            ob_start();
            require("html/front-page-content.html");
            $content = ob_get_clean();
        }
        return $content;
    }

    /**
     * @return bool
     */
    public function isPageOnFront()
    {
        return is_page(get_option('page_on_front'));
    }
}