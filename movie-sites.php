<?php

/*
Plugin Name: Movies
Plugin URI: http://nodesman.com/
Description: A wordpress plugin to show your favorite movies on your front page.
Version: 0.1
Author: Raj
Author URI: http://nodesman.com/
*/
require('MoviePostTypeManager.php');
require('FrontPageRenderer.php');


class Movies
{
    private $moviePostTypeManager;
    private $frontPageRenderer;

    function __construct()
    {
        $this->moviePostTypeManager = new MoviePostTypeManager();
        add_action('admin_notices', array($this, 'misconfigured_notification'));

        if (self::configuredForMovieListing()) {
            $this->frontPageRenderer = new FrontPageRenderer();
        }
    }

    function misconfigured_notification()
    {
        if (!self::configuredForMovieListing()) {
            require("html/admin_error.html");
        }
    }

    public static function configuredForMovieListing()
    {
        $front_page = intval(get_option('page_on_front'));
        return (0 !== $front_page);
    }


}

$movie = new Movies();