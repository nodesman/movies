(function () {

    describe("movieService", function () {
        var $httpBackend, $host;
        beforeEach(function () {
            module('MovieApp');
            angular.module('MovieApp').value('rootUrl', 'http://localhost/moxie')
        });
        var $movieService;
        describe(".fetchMovies()", function () {
            beforeEach(inject(function (movieService, $injector) {
                $host = {
                    callback: function (data, error) {
                    }
                };
                spyOn($host, 'callback');
                $movieService = movieService;
                $httpBackend = $injector.get('$httpBackend');
            }));

            it("makes the expected AJAX call", function () {
                $httpBackend.whenGET('http://localhost/moxie/?movies=1').respond({
                    data: [
                        {
                            id: 1,
                            title: "Up",
                            poster_url: "http://localhost.dev/images/up.jpg",
                            rating: 5,
                            year: 2010,
                            short_description: "Phasellus ultrices nulla quis nibh. Quisque a lectus",
                        }
                    ]
                });
                $httpBackend.expectGET('http://localhost/moxie/?movies=1');
                $movieService.fetchMovies($host.callback);
                expect($httpBackend.flush).not.toThrow();
            });

            afterEach(function () {
                $httpBackend.verifyNoOutstandingExpectation();
                $httpBackend.verifyNoOutstandingRequest();
            });
        });
    });
}());