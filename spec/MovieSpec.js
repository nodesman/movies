describe("The front-end movie controller", function () {
    var $scope, $movieController, $service;

    beforeEach(function () {
        module('MovieApp');
    });

    describe("when instantiated", function () {

        beforeEach(inject(function ($rootScope, $controller) {
            $scope = $rootScope.$new();
            $service = {};
            $service.fetchMovies = function () {
                return {};
            };
            spyOn($service, 'fetchMovies');
            $movieController = $controller('MovieController', {
                '$scope': $scope,
                'movieService': $service
            });
        }));


        it('shows the loading gif', function () {
            expect($scope.loading).toBe(true);
        });

        it('makes the AJAX request to fetch movies', function () {
            expect($service.fetchMovies).toHaveBeenCalled();
        });
    });


    describe("when given the right AJAX response", function () {
        beforeEach(inject(function ($rootScope, $controller) {
            $scope = $rootScope.$new();
            $service = {};
            $service.fetchMovies = function (callback) {

            };
            $movieController = $controller('MovieController', {
                '$scope': $scope,
                'movieService': $service
            });
        }));

        it("creates the right view model", function () {
            $scope.ajaxCallback({
                data: [
                    {
                        id: 1,
                        title: "Up",
                        poster_url: "http://localhost.dev/images/up.jpg",
                        rating: 5,
                        year: 2010,
                        short_description: "Phasellus ultrices nulla quis nibh. Quisque a lectus",
                    },
                    {
                        id: 2,
                        title: "Avatar",
                        poster_url: "http://localhost.dev/images/up.jpg",
                        rating: 2,
                        year: 2010,
                        short_description: "Phasellus ultrices nulla quis nibh. Quisque a lectus",
                    }
                ]
            });

            expect($scope.list).toEqual([
                {
                    id: 1,
                    title: "Up",
                    poster_url: "http://localhost.dev/images/up.jpg",
                    rating: [1, 1, 1, 1, 1],
                    year: 2010,
                    short_description: "Phasellus ultrices nulla quis nibh. Quisque a lectus",
                }, {
                    id: 2,
                    title: "Avatar",
                    poster_url: "http://localhost.dev/images/up.jpg",
                    rating: [1, 1],
                    year: 2010,
                    short_description: "Phasellus ultrices nulla quis nibh. Quisque a lectus",
                }
            ]);
        });
    });


});