<?php

/**
 * Created by IntelliJ IDEA.
 * User: raj
 * Date: 10/31/15
 * Time: 4:07 PM
 */
class MoviePostTypeManager
{
    public function __construct()
    {
        add_action('init', array($this, 'init'));
        add_action('load-post-new.php', array($this, 'enqueue_style'));
        add_action('load-post.php', array($this, 'load_movie'));
        add_action('save_post', array($this, 'save_post'));
    }

    public function init()
    {
        $args = array(
            'labels' => array(
                'name' => 'Movies',
                'singular_name' => 'Movie',
            ),
            'public' => true,
            'menu_position' => 5,
            'supports' => array(
                'title', 'excerpt'
            ),
            'register_meta_box_cb' => array($this, 'register_meta_boxes')
        );
        register_post_type('movie', $args);
    }

    function register_meta_boxes()
    {
        add_meta_box('movie_fields', 'Movie Details', array($this, 'render_meta_box'));
    }

    function render_meta_box($post)
    {
        $values = get_post_custom($post->ID);
        require('html/post_meta_box.html');
    }

    function load_movie()
    {
        $this->enqueue_style();
    }

    function save_post($post_id)
    {
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return;
        }
        if (!isset($_POST['movie_url']) || !wp_verify_nonce($_POST['movie_nonce'], 'author-movie')) {
            return;
        }

        $movieYear = intval($_POST['movie_year']);
        $movieRating = intval($_POST['movie_rating']);
        $movieUrl = $_POST['movie_url'];
        update_post_meta($post_id, "movie_year", $movieYear);
        update_post_meta($post_id, "movie_rating", $movieRating);
        update_post_meta($post_id, "movie_url", $movieUrl);
    }

    function enqueue_style()
    {
        wp_enqueue_style('movie-admin-style', plugin_dir_url(__FILE__) . 'css/admin.css');
    }


}